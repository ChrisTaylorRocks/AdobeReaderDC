<#
.SYNOPSIS
    This script will download the latest version of Adobe ReaderDC and install. Then install the latest update.

.OUTPUTS
    Adobe ReaderDC

.NOTES
    Version:        1.0
    Author:         Chris Taylor
    Creation Date:  6/1/2017
    Purpose/Change: Initial script development

.LINK
  christaylor.codes

#>

$FTPBase = 'ftp://ftp.adobe.com/pub/adobe/reader/win/AcrobatDC/'

function Get-FTPDirectories {
    param(
    $FTPAddress
    )

    [System.Net.FtpWebRequest]$ftp = [System.Net.WebRequest]::Create($FTPAddress) 
    $ftp.Method = [System.Net.WebRequestMethods+FTP]::ListDirectory#Details
    $response = $ftp.getresponse() 
    $stream = $response.getresponsestream()

    $buffer = new-object System.Byte[] 1024 
    $encoding = new-object System.Text.AsciiEncoding 

    $outputBuffer = "" 
    $foundMore = $false 

    do{ 
        start-sleep -m 1000 

        $foundmore = $false 
        $stream.ReadTimeout = 1000

        do{ 
            try{ 
                $read = $stream.Read($buffer, 0, 1024) 

                if($read -gt 0){ 
                    $foundmore = $true 
                    $outputBuffer += ($encoding.GetString($buffer, 0, $read)) 
                } 
            } catch { $foundMore = $false; $read = 0 } 
        } while($read -gt 0) 
    } while($foundmore)

    $outputBuffer =  ($outputBuffer -split '[\r\n]') |? {$_}
    [array]$FileNames = @()
    foreach ($File in $outputBuffer){
        $FileNames += ($File -split '/')[-1]
    }

    Write-Output $FileNames
}

$Versions = Get-FTPDirectories -FTPAddress $FTPBase | where{$_ -match '^\d*$'} | Sort-Object -Descending

#Find latest installer
Write-Output "Looking for latest installer.."
$VersionLoop = 0
while(!$Check){
        $FilesInDir = Get-FTPDirectories -FTPAddress $($FTPBase + $Versions[$VersionLoop])
        $Check = $FilesInDir | where {$_ -like "*_en_US.msi"}
        $VersionLoop ++        
        if ($Versions.count -lt $VersionLoop) {
            $Check = 'Error'
        }
}
$VersionLoop --
Write-Output "Latest installer, $Check"

#Download the latest installer
Write-Output "Downloading.."
$webclient = New-Object System.Net.WebClient 
$webclient.DownloadFile($($FTPBase + $Versions[$($VersionLoop)] + '/' + $Check), "$env:temp\$Check")
#Install
Write-Output "Installing.."
Get-Process msiexec -ErrorAction SilentlyContinue | Stop-Process -Force
Start-Process -FilePath msiexec -ArgumentList "/i $env:temp\$Check /l $env:temp\$Check.log /qn"
Write-Output "Adobe Reader DC, Installed"

#Find latest Update
Write-Output "Looking for latest update.."
Remove-Variable Check
$VersionLoop = 0
while(!$Check){
        $FilesInDir = Get-FTPDirectories -FTPAddress $($FTPBase + $Versions[$VersionLoop])
        $Check = $FilesInDir | where {$_ -like "*.msp" -and $_ -notlike "*_MUI.msp"}
        $VersionLoop ++        
}
$VersionLoop --
Write-Output "Latest update, $Check"

#Download update
Write-Output "Downloading.."
$webclient = New-Object System.Net.WebClient 
$webclient.DownloadFile($($FTPBase + $Versions[$($VersionLoop)] + '/' + $Check), "$env:temp\$Check")
#Install update
Write-Output "Installing.."
Get-Process msiexec -ErrorAction SilentlyContinue | Stop-Process -Force
Start-Process -FilePath msiexec -ArgumentList "/p $env:temp\$Check /l $env:temp\$Check.log /qn"
Write-Output "Adobe Reader DC, Update Installed"